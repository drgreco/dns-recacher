# dns-recacher
ideas on a caching dns server that is useful for a small user-set

this project aims to be a service, or set of services that ideally run on a local network

* keep 90% (configurable) of cache active before TTL expires
* ability to query root servers directly, or random list/set of public servers.
* allow for sanity check of dns
  * check against a secondary dns server on all requests that will return a known response to identify DNS hijacking\
  * query multiple dns sources per query
  * ignore if response qualified with DNSSEC


